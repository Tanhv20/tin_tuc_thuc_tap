<!DOCTYPE html>
<html lang="en">
<?php
require_once ('header.html.php');
require_once ('model.php');
?>
<content>
    <div class="new">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 item-new item-new-left">
                    <div id="news-1" class="owl-carousel owl-theme">
                        <?php foreach ($dataHots as $data) {?>

                                <div class="item item-before " data-ll-status="loaded">
                                    <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"><img src="<?php echo $data['url_thumbnail']?>"></a>
                                    <div class="overlay-post-content">
                                        <div class="post-content">
                                            <div class="grid-category">
                                                <a class="post-cat" href="" style="background-color:#da1793;color:#ffffff"> <?php echo $data['tag'] ?></a>
                                                <a class="post-cat" href="" style="background-color:#fda400;color:#ffffff"> <?php echo $data['categorie'] ?></a>
                                            </div>
                                            <h3 class="post-title"> <a href=""> <?php echo $data['title'] ?></a></h3>
                                            <ul class="post-meta-info">
                                                <li class="author"> <i class="fa fa-user"></i> <a href=""> <?php echo $data['author']?> </a></li>
                                                <li> <i class="fa fa-clock-o"></i> <?php echo $data['time'] ?></li>
                                                <li class="active"> <i class="icon icon-fire"></i> 2955</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                        <?php }?>
                    </div>
                </div>
                <!-- col-lg-8 item-new item-new-left -->
                <div class="col-lg-4 item-new-right">
                    <?php foreach ($dataLimit as $data){?>
                        <div class="item-top">
                            <div class="item item-before rocket-lazyload lazyloaded" data-ll-status="loaded">
                                <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"><img src="<?php echo $data['url_thumbnail']?>"></a>
                                <div class="overlay-post-content">
                                    <div class="post-content">
                                        <div class="grid-category"> <a class="post-cat" href="" style="background-color:#da1793;color:#ffffff"> <?php echo $data['tag']?>
                                            </a> <a class="post-cat" href="https://demo.themewinter.com/wp/digiqoles/category/lifestyle/" style="background-color:#fda400;color:#ffffff"><?php $data['categorie']?></a>
                                        </div>
                                        <h3 class="post-title"> <a href=""><?php echo $data['title']?></a></h3>
                                        <ul class="post-meta-info">
                                            <li class="author"> <i class="fa fa-user"></i> <a href=""> <?php echo $data['author']?></a></li>
                                            <li> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                                            <li class="active"> <i class="icon icon-fire"></i> 2955</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php foreach ($dataNewLs as $data){?>
                        <div class="item-bottom">
                            <div class="item" data-ll-status="loaded">
                                <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"><img src="<?php echo $data['url_thumbnail']?>"></a>
                                <div class="overlay-post-content">
                                    <div class="post-content">
                                        <div class="grid-category"> <a class="post-cat" href="" style="background-color:#da1793;color:#ffffff"><?php echo $data['tag'] ?></a> <a class="post-cat" href="" style="background-color:#fda400;color:#ffffff"><?php $data['categorie']?></a></div>
                                        <h3 class="post-title"> <a href=""><?php echo $data['title']?></a></h3>
                                        <ul class="post-meta-info">
                                            <li class="author"> <i class="fa fa-user"></i> <a href=""> <?php echo $data['author']?></a></li>
                                            <li> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                            <li class="active"> <i class="icon icon-fire"></i> 2955</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- new -->
    <div class="new-sports" style="padding: 40px 0">
        <div class="container">
            <div class="row">
                <div id="slider-sports" class= "slider owl-carousel owl-theme" style="width: 100%;">
                    <?php foreach ($dataTTs as $data ){?>
                        <div class="item-slider  ">
                            <div class="ts-overlay-style ">
                                <div class="item item-before " href="detail.php?id=<?php echo $data["id_new"];  ?> " style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                    <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                    <div class="overlay-post-content">
                                        <div class="post-content">
                                            <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"><?php echo $data['tag']?> </a></div>
                                            <h3 class="post-title"> <a href=""> <?php echo $data['title']?> </a></h3>
                                            <ul class="post-meta-info ">
                                                <li> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- item-slider -->
                    <?php }?>
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- new-sports -->
    <section>
        <div class="new-page2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="new-page2-left">
                            <div class="section-heading heading-style3">
                                <h2 class="block-title">
                                    <span class="title-angle-shap"> what’s new </span>
                                </h2>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd4bc5ea88" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">All</span></h3>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd4407292c" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">Lifestyle</span></h3>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd49ee1509" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">Travel</span></h3>
                                    </a>
                                </li>
                            </ul>
                            <!--  tab -->
                            <div class="tab-content">
                                <div class="col-md-6" style="padding-left: 0">
                                    <?php foreach ($dataLimit as $data){?>
                                        <div class="item item-before" >
                                            <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                                <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                                <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"><?php echo $data['tag']?> </a></div>
                                            </div>
                                            <div class="post-content">
                                                <div class="post-content">
                                                    <h3 class="post-title"> <a href=""><?php echo $data['title']?> </a></h3>
                                                    <ul class="post-meta-info ">
                                                        <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $dataL['author']?></a> </span>
                                                        <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                                    </ul>
                                                    <p><?php echo $data['content']?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                                <div class="col-md-6 list-new">
                                    <?php foreach ($dataHots as $data){ ?>
                                        <div class="item-new">
                                            <div class="img img-right" style="background-image: url('<?php echo $data['url_thumbnail']?>');"></div>
                                            <div class="post-content">
                                                <div class="media-body">
                                                    <a class="post-cat only-color" href="detail.php?id=<?php echo $data["id_new"];  ?>" style="color: #007bff;"> <?php echo $data['categorie']?></a>
                                                    <h4 class="post-title title-small">
                                                        <a href="" rel="bookmark" title="Naturalistic a design is thriv as actual nature dies"><?php echo $data['title']?></a>
                                                    </h4>
                                                    <div class="post-meta">
                                                        <span class="post-date"> <i class="fa fa-clock-o"></i><?php echo $data['time']?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                        <!-- new-page2-left -->
                        <div class="new-page3">
                            <div class="elementor-widget-container">
                                <div class="section-heading heading-style3">
                                    <h2 class="block-title"> <span class="title-angle-shap"> Lifestyle </span></h2>
                                </div>
                            </div>
                            <div class="col-md-6 left-item">
                                <?php foreach (  $datanewsCN as $data){?>
                                    <div class="item item-before">
                                        <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                            <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                            <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"> <?php echo $data['tag']?> </a></div>
                                        </div>
                                        <div class="post-content">
                                            <div class="post-content">
                                                <h3 class="post-title"> <a href=""><?php echo $data['title']?> </a></h3>
                                                <ul class="post-meta-info ">
                                                    <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $dataCNT['author']?></a> </span>
                                                    <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="col-md-6 right-item">
                                <?php foreach ($datanewsCN4 as $data){?>
                                    <div class="item item-before">
                                        <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                            <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                            <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"><?php echo $data['tag']?></a></div>
                                        </div>
                                        <div class="post-content">
                                            <div class="post-content">
                                                <h3 class="post-title"> <a href=""><?php echo $data['title']?></a></h3>
                                                <ul class="post-meta-info ">
                                                    <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- col-md-8 -->
                    <div class="col-md-4 side-bar">
                        <div class="section-heading heading-style3">
                            <h2 class="block-title"> <span class="title-angle-shap"> Follow us </span></h2>
                        </div>
                        <div class="elementor-element" data-id="5164de3d" data-element_type="widget" data-widget_type="wp-widget-apsc_widget.default">
                            <div class="elementor-widget-container">
                                <div class="apsc-icons-wrapper clearfix apsc-theme-2">
                                    <div class="apsc-each-profile">
                                        <a class="apsc-facebook-icon clearfix" href="https://facebook.com/xpeedstudio" target="_blank">
                                            <div class="apsc-inner-block">
                                                <span class="social-icon"><i class="fa fa-facebook" aria-hidden="true"></i><span class="media-name">Facebook</span></span> <span class="apsc-count">8,047</span><span class="apsc-media-type">Fans</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="apsc-each-profile">
                                        <a class="apsc-twitter-icon clearfix" href="https://twitter.com/xpeedstudio" target="_blank">
                                            <div class="apsc-inner-block">
                                       <span class="social-icon">
                                       <i class="fa fa-twitter" aria-hidden="true"></i><span class="media-name">Twitter</span></span> <span class="apsc-count">502</span><span class="apsc-media-type">Followers</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="apsc-each-profile">
                                        <a class="apsc-instagram-icon clearfix" href="https://instagram.com/xpeeder" target="_blank">
                                            <div class="apsc-inner-block">
                                                <span class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i><span class="media-name">Instagram</span></span> <span class="apsc-count">302</span><span class="apsc-media-type">Followers</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="apsc-each-profile">
                                        <a class="apsc-youtube-icon clearfix" href="https://www.youtube.com/channel/UCJp-j8uvirVgez7TDAmfGYA" target="_blank">
                                            <div class="apsc-inner-block">
                                                <span class="social-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i><span class="media-name">Youtube</span></span> <span class="apsc-count">644</span><span class="apsc-media-type">Subscriber</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- elementor-widget-container -->
                        </div>
                        <div class="elementor-element " data-id="4c57ac0" data-element_type="widget" data-widget_type="image.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-image">
                                    <a href="#">
                                        <img width="330" height="306" src="img/sidebar_banner.png" class="attachment-full size-full lazyloaded" alt="" data-ll-status="loaded">
                                        <noscript><img width="330" height="306" src="https://demo.themewinter.com/wp/digiqoles/wp-content/uploads/2020/07/sidebar_banner.png" class="attachment-full size-full" alt="" /></noscript>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-new-sibar list-new">
                            <ul class="nav nav-tabs tabs-sibar" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd4bc5ea88" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">All</span></h3>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd4407292c" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">Lifestyle</span></h3>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#7fc4cdd49ee1509" data-toggle="tab">
                                        <h3 class="tab-head"><span class="tab-text-title">Travel</span></h3>
                                    </a>
                                </li>
                            </ul>
                            <?php foreach ($datas3 as $data){?>
                                <div class="item-new">
                                    <div class="img img-right" style="background-image: url('<?php echo $data['url_thumbnail']?>');"></div>
                                    <div class="post-content">
                                        <div class="media-body">
                                            <a class="post-cat only-color" href="detail.php?id=<?php echo $data["id_new"];  ?>" style="color: #007bff;"><?php echo $data['categorie']?></a>
                                            <h4 class="post-title title-small">
                                                <a href="" rel="bookmark" title="Naturalistic a design is thriv as actual nature dies"><?php echo $data['title']?></a>
                                            </h4>
                                            <div class="post-meta">
                                                <span class="post-date"> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                    <!-- side-bar -->
                </div>
            </div>
        </div>
    </section>
    <div class="new-sports new-sports-2" style="padding: 40px 0">
        <div class="container">
            <div class="row">
                <div id="slider-sports-2" class="slider owl-carousel owl-theme" style="width: 100%;">
                    <?php foreach($datanews as $data){ ?>
                        <div class="item item-before ">
                            <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"><?php echo $data['categorie']?> </a></div>
                            </div>
                            <div class="post-content">
                                <div class="post-content">
                                    <h3 class="post-title"> <a href=""> <?php echo $data['title']?> </a></h3>
                                    <ul class="post-meta-info ">
                                        <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $datanew['author']?> </a></h3></a> </span>
                                        <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <div class="news-page3">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ">
                    <div class="section-heading heading-style3">
                        <h2 class="block-title"> <span class="title-angle-shap"> Tech </span></h2>
                    </div>
                    <?php foreach ($datanewsCN4 as $data){?>
                        <div class="item item-before item-new-page3">
                            <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                <a class="img-link" href=""></a>
                            </div>
                            <div class="post-content">
                                <div class="grid-cat"> <a class="post-cat" href="" style="background-color:#4ca80b;color:#ffffff"><?php echo $data['tag']?></a></div>
                                <h3 class="post-title"> <a href=""> <?php echo $data['title']?></a></h3>
                                <ul class="post-meta-info ">
                                    <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $dataCN4['author']?></a> </span>
                                    <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i><?php echo $data['time']?></li>
                                </ul>
                                <p><?php echo $data['content']?></p>
                            </div>
                        </div>
                    <?php }?>
                    <div class="page3-bottom ">
                        <?php foreach ($datanewsCN4 as $data){?>
                            <div class="item item-before item-new-page3">
                                <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                    <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                </div>
                                <div class="post-content">
                                    <div class="grid-cat"> <a class="post-cat" href="" style="background-color:#4ca80b;color:#ffffff"><?php echo $data['tag']?> </a></div>
                                    <h3 class="post-title"> <a href=""><?php echo $data['title']?></a></h3>
                                    <ul class="post-meta-info ">
                                        <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                                    </ul>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <!-- col-md-8 -->
                <div class="col-md-4">
                    <div class="category-sibar">
                        <div class="" data-id="36881ca2" data-element_type="widget">
                            <div class="elementor-widget-container">
                                <div class="ts-category ts-category-list-item">
                                    <ul class="ts-category-list">
                                        <?php $i=0;foreach ($dataMenu as $menu){ $i++;?>
                                            <li style="background-image: url('img/travel_4.jpg');">
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%;">
                                                        <td style="width: 45%;"> <a><?php echo $menu['menu']?></a></td>
                                                        <td style="width: 35%;"><span class="bar">------------</span></td>
                                                        <td style="width: 25%;"><span class="category-count"><? echo $i;?></span></td>
                                                    </tr>
                                                </table>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- category-sibar -->
                </div>
            </div>
            <div class="qc col-md-12">
                <a href="">
                    <img src="img/content_banner_3.png" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="new-page4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 list-item">
                    <div class="section-heading heading-style3">
                        <h2 class="block-title">
                            <span class="title-angle-shap"> what’s new </span>
                        </h2>
                    </div>
                    <?php foreach ($datas as $data){?>
                        <div class="item item-before " style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                            <a class="img-link" href=""></a>
                            <div class="overlay-post-content">
                                <div class="post-content">
                                    <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"> <?php echo $data['categorie']?> </a></div>
                                    <h3 class="post-title"> <a href=""> <?php echo $data['title']?> </a></h3>
                                    <ul class="post-meta-info ">
                                        <li> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <div class="col-md-4 side-bar">
                    <div class="section-heading heading-style3">
                        <h2 class="block-title">
                            <span class="title-angle-shap"> what’s new </span>
                        </h2>
                    </div>
                    <div class="list-new">
                        <?php foreach ($datanewsCN4 as $data){  ?>
                            <div class="item-before item-new-page3">
                                <div class="img" style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                                    <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                                </div>
                                <div class="post-content">
                                    <div class="grid-cat"> <a class="post-cat" href="" style="background-color:#4ca80b;color:#ffffff"><?php echo $data['categorie']?></a></div>
                                    <h3 class="post-title"> <a href=""> <?php echo $data['title']?></a></h3>
                                    <ul class="post-meta-info ">
                                        <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                                    </ul>
                                </div>
                            </div>
                        <?php }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="slider-page4" class="slider-page4 owl-carousel owl-theme">
        <?php foreach ($datas as $data){?>
            <div class="item item-before " style="background-image: url('<?php echo $data['url_thumbnail']?>');">
                <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];  ?>"></a>
                <div class="overlay-post-content">
                    <div class="post-content">
                        <div class="grid-category"> <a class="post-cat" href="" style="background-color:#f3670a;"> <?php echo $data['categorie']?> </a></div>
                        <h3 class="post-title"> <a href=""> <?php echo $data['title']?> </a></h3>
                        <ul class="post-meta-info ">
                            <li> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</content>
<mail>
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-5 align-self-center">
                    <div class="footer-logo">
                        <a class="logo" href="" alt="DigiQole" data-ll-status="loaded">
                            <img class="img-fluid" src="img/logo-light.png">
                            <noscript><img class="img-fluid" src="img/logo-light.png"></noscript>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7">
                    <div class="ts-subscribe">
                        <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-464" method="post" data-id="464" data-name="footer newsletter">
                            <div class="mc4wp-form-fields">
                                <div class="footer-newsletter">
                                    <p> <i class="fa fa-paper-plane" aria-hidden="true"></i> <input type="email" name="EMAIL" placeholder="Your email" required=""></p>
                                    <input type="submit" value="Subscribe">
                                </div>
                            </div>
                            <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1597701941"><input type="hidden" name="_mc4wp_form_id" value="464"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                            <div class="mc4wp-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</mail>
<?php require_once ('footer.html.php')?>
</body>
</html>