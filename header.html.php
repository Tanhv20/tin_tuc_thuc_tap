<?php
//Kết nối databse
$con = mysqli_connect('localhost', 'root', '', 'tin_tuc_user');
//Viết câu SQL lấy tất cả dữ liệu trong bảng players
mysqli_set_charset($con,'utf8');
$sql="SELECT * FROM `menu` ORDER BY `id_menu`";
//Chạy câu SQL
$resultMenu=mysqli_query($con,$sql);
//Gắn dữ liệu lấy được vào mảng $data

while ($row=mysqli_fetch_assoc($resultMenu)) {
    $dataMenu[] = $row;

}
//$ai=0;
//$a= array();
//foreach ($dataMenu as $menu){
//    $a[$ai]=$menu['menu'];
//    $ai++;
//}
//echo $a[2];

$sqlHot ="SELECT * FROM `post` where 'the_loai' like 'hot' ";
$resultHot= mysqli_query($con,$sqlHot);
while ($row=mysqli_fetch_assoc($resultHot)) {
    $dataHot[] = $row;
}

?>
<html>

<?php
require_once ('model.php');
require_once ('include.php')?>

<body>
    <header>
        <div class="topbar topbar-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="tranding-bg-white">
                            <div class="tranding-bar">
                                <div id="tredingcarousel" class="trending-slide carousel slide trending-slide-bg" data-ride="carousel">
                                    <p class="trending-title"><i class="fa fa-bolt">
                                        </i> Trending
                                    </p>
                                    <div class="carousel-inner">
                                        <div class="carousel-item">
                                            <div class="post-content">
                                                <p class="post-title title-small">
                                                    <a href="#">Ratcliffe to be Director of nation talent Trump ignored</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 xs-center align-self-center text-right">
                        <ul class="top-info">
                            <li> <i class="fa fa-calendar-check-o" aria-hidden="true"></i> August 17, 2020</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-3 align-self-center">
                        <div class="logo-area">
                            <a rel="home" class="logo" href="">
                                <img class="img-fluid lazyloaded" src="img/logo.png" alt="DigiQole" data-ll-status="loaded">
                                <noscript><img class="img-fluid" src="img/logo.png"></noscript>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-9 align-self-center">
                        <div class="banner-img text-right">
                            <a href="#" target="_blank">
                                <img class="img-fluid lazyloaded" src="img/header_banner.png" >
                                <noscript>
                                    <img class="img-fluid" src="img/header_banner.png" alt="Digiqole ads">
                                </noscript>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-mobile" style="background-image: linear-gradient(20deg, #f84270 0%, #fe803b 100%);">
            <div class="container">
                <div class="row">
                    <span class="bars" id="bars"><i class="fa fa-bars" aria-hidden="true"></i></span>
                    <form class="seach-mobile" method="get" action="search.php">
                        <input type="text" name="search-mobile">
                        <button class="input-group-btn search-button" type="submit" name="search-button">
                            <i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav " id="responsive-menu">
                                <?php foreach ($dataMenu as $menu){?>
                                    <li class="menu-item" ><a href="<?php echo $menu['linkmenu']?>.php"><?php echo $menu['menu']?></a></li>
                                <?php } ?>
                            </ul>

                            <ul class="social-links text-right">
                                <li class=""> <a target="_blank" title="facebook" href="#"> <span class="social-icon"> <i class="fa fa-facebook" aria-hidden="true"></i> </span> </a></li>
                                <li class=""> <a target="_blank" title="Twitter" href="#"> <span class="social-icon"> <i class="fa fa-twitter" aria-hidden="true"></i> </span> </a></li>
                                <li class=""> <a target="_blank" title="zalo" href="#"> <span class="social-icon"> <i class="fa fa-instagram" aria-hidden="true" ></i> </span> </a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="menu">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav " >
                                <?php foreach ($dataMenu as $menu){?>
                                    <li class="menu-item" ><a href="<?php echo $menu['linkmenu']?>.php"><?php echo $menu['menu']?></a></li>
                                <?php } ?>
                            </ul>

                            <ul class="social-links text-right">
                                <li class=""> <a target="_blank" title="facebook" href="#"> <span class="social-icon"> <i class="fa fa-facebook" aria-hidden="true"></i> </span> </a></li>
                                <li class=""> <a target="_blank" title="Twitter" href="#"> <span class="social-icon"> <i class="fa fa-twitter" aria-hidden="true"></i> </span> </a></li>
                                <li class=""> <a target="_blank" title="zalo" href="#"> <span class="social-icon"> <i class="fa fa-instagram" aria-hidden="true" ></i> </span> </a></li>
                            </ul>
                            <div class="nav-search-area">



                                <form method="get" action="search.php" class="header-search-icon">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search-mobile" placeholder="Search Keyword" value="">
                                        <button class=" search-button" type="submit" name="search-button">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</body>
</html>