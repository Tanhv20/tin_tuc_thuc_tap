<?php require_once ('header.html.php');
require_once ('model.php');
$sql="SELECT * FROM `post` where `tag` like 'Thời trang'";
//Chạy câu SQL

$resultsthoitrang=mysqli_query($con,$sql);

//Gắn dữ liệu lấy được vào mảng $data
while ($row=mysqli_fetch_assoc($resultsthoitrang)) {
    $datathoitrang[] = $row;
}
?>

<body>
<contents>
    <div class="new-page2">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="new-page2-left">
                        <div class="section-heading heading-style3">
                            <h2 class="block-title">
                                <span class="title-angle-shap"> what’s new </span>
                            </h2>
                        </div>
                        <div class="tab-content">
                            <div class=" list-new list-new-of-list-item">
                                <?php if(empty($datathoitrang)){ echo "không có bài viết ở danh mục này";}else { foreach ($datathoitrang as $data){?>
                                    <div class="item item-before item-new-page3">
                                        <div class="img" style="background-image: url('<?php echo $data['url_thumbnail'] ?>')">
                                            <a class="img-link" href="detail.php?id=<?php echo $data["id_new"];?>"></a>
                                        </div>
                                        <div class="post-content">
                                            <div class="grid-cat"> <a class="post-cat" href="" style="background-color:#4ca80b;color:#ffffff"><?php echo $data['tag'] ?> </a></div>
                                            <h3 class="post-title"> <a href="detail.php?id=<?php echo $data["id_new"];?>"> <?php echo $data['title'] ?> </a></h3>
                                            <ul class="post-meta-info ">
                                                <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $data['author']?></a> </span>
                                                <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i> <?php echo $data['time'] ?> </li>
                                            </ul>
                                            <p><?php echo $data['content'] ?> </p>
                                        </div>
                                    </div>
                                <?php } }?>
                            </div>
                        </div>
                    </div><!-- new-page2-left -->
                </div> <!-- col-md-8 -->
                <?php require_once ('sideBar.php');?>
            </div>
        </div>
    </div>
</contents>
<mail>
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-5 align-self-center">
                    <div class="footer-logo">
                        <a class="logo" href="" alt="DigiQole" data-ll-status="loaded">
                            <img class="img-fluid" src="img/logo-light.png">
                            <noscript><img class="img-fluid" src="img/logo-light.png"></noscript>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7">
                    <div class="ts-subscribe">

                        <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-464" method="post" data-id="464" data-name="footer newsletter">
                            <div class="mc4wp-form-fields">
                                <div class="footer-newsletter">
                                    <p> <i class="fa fa-paper-plane" aria-hidden="true"></i> <input type="email" name="EMAIL" placeholder="Your email" required=""></p>
                                    <input type="submit" value="Subscribe">
                                </div>
                            </div>
                            <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1597701941"><input type="hidden" name="_mc4wp_form_id" value="464"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                            <div class="mc4wp-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</mail>
<?php require_once ('footer.html.php')?>
</body>
</html>