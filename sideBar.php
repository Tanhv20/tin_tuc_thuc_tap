<div class="col-md-4 side-bar">
    <div class="section-heading heading-style3"><h2 class="block-title"> <span class="title-angle-shap"> Follow us </span></h2></div>
    <div class="elementor-element" data-id="5164de3d" data-element_type="widget" data-widget_type="wp-widget-apsc_widget.default">
        <div class="elementor-widget-container">
            <div class="apsc-icons-wrapper clearfix apsc-theme-2">
                <div class="apsc-each-profile">
                    <a class="apsc-facebook-icon clearfix" href="https://facebook.com/xpeedstudio" target="_blank">
                        <div class="apsc-inner-block">
                            <span class="social-icon"><i class="fa fa-facebook" aria-hidden="true"></i><span class="media-name">Facebook</span></span> <span class="apsc-count">8,047</span><span class="apsc-media-type">Fans</span>
                        </div>
                    </a>
                </div>
                <div class="apsc-each-profile">
                    <a class="apsc-twitter-icon clearfix" href="https://twitter.com/xpeedstudio" target="_blank">
                        <div class="apsc-inner-block">
                                                <span class="social-icon">
                                                   <i class="fa fa-twitter" aria-hidden="true"></i><span class="media-name">Twitter</span></span> <span class="apsc-count">502</span><span class="apsc-media-type">Followers</span>
                        </div>
                    </a>
                </div>
                <div class="apsc-each-profile">
                    <a class="apsc-instagram-icon clearfix" href="https://instagram.com/xpeeder" target="_blank">
                        <div class="apsc-inner-block">
                            <span class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i><span class="media-name">Instagram</span></span> <span class="apsc-count">302</span><span class="apsc-media-type">Followers</span>
                        </div>
                    </a>
                </div>
                <div class="apsc-each-profile">
                    <a class="apsc-youtube-icon clearfix" href="https://www.youtube.com/channel/UCJp-j8uvirVgez7TDAmfGYA" target="_blank">
                        <div class="apsc-inner-block">
                            <span class="social-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i><span class="media-name">Youtube</span></span> <span class="apsc-count">644</span><span class="apsc-media-type">Subscriber</span>
                        </div>
                    </a>
                </div>
            </div>
        </div> <!-- elementor-widget-container -->
    </div>
    <div class="elementor-element " data-id="4c57ac0" data-element_type="widget" data-widget_type="image.default"><div class="elementor-widget-container"><div class="elementor-image"> <a href="#"> <img width="330" height="306" src="img/sidebar_banner.png" class="attachment-full size-full lazyloaded" alt="" data-ll-status="loaded"><noscript><img width="330" height="306" src="https://demo.themewinter.com/wp/digiqoles/wp-content/uploads/2020/07/sidebar_banner.png" class="attachment-full size-full" alt="" /></noscript> </a></div></div></div>
    <div class="list-new-sibar list-new">
        <ul class="nav nav-tabs tabs-sibar" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#7fc4cdd4bc5ea88" data-toggle="tab">
                    <h3 class="tab-head"><span class="tab-text-title">All</span></h3>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#7fc4cdd4407292c" data-toggle="tab">
                    <h3 class="tab-head"><span class="tab-text-title">Lifestyle</span></h3>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#7fc4cdd49ee1509" data-toggle="tab">
                    <h3 class="tab-head"><span class="tab-text-title">Travel</span></h3>
                </a>
            </li>

        </ul>

        <?php foreach ($datas3 as $data){?>
            <a href="detail.php?id=<?php echo $data["id_new"];  ?>">
                <div class="item-new">
                    <div class="img img-right" style="background-image: url('<?php echo $data['url_thumbnail']?>');"></div>
                    <div class="post-content">
                        <div class="media-body">
                            <a class="post-cat only-color" href="" style="color: #007bff;"><?php echo $data['categorie']?></a>
                            <h4 class="post-title title-small">
                                <a href="detail.php?id=<?php echo $data["id_new"];?>" rel="bookmark" title="Naturalistic a design is thriv as actual nature dies"><?php echo $data['title']?></a>
                            </h4>
                            <div class="post-meta">
                                <span class="post-date"> <i class="fa fa-clock-o"></i> <?php echo $data['time']?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

        <?php }?>
    </div>
    <div class="category-sibar">
        <div class="" data-id="36881ca2" data-element_type="widget">
            <div class="elementor-widget-container">
                <div class="ts-category ts-category-list-item">
                    <ul class="ts-category-list">
                        <?php $i=0;foreach ($dataMenu as $dtmenu){ $i++;?>
                            <li style="background-image: url('img/travel_4.jpg');">
                                <table style="width: 100%;">
                                   <a href="<?php echo $dtmenu['menu']?>.php">
                                       <tr style="width: 100%; height: 100px; line-height: 100px">
                                        <td style="width: 45%;"> <a href="<?php echo $dtmenu['linkmenu']?>.php" ><?php echo $dtmenu['menu']?></a></td>
                                        <td style="width: 35%;"><span class="bar">------------</span></td>
                                        <td style="width: 25%;"><span class="category-count"><? echo $i;?></span></td>
                                    </tr>
                                   </a>
                                </table>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- category-sibar -->
</div> <!-- side-bar -->
