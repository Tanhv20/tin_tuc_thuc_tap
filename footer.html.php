<footer class="ts-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 fadeInUp">
                <div class="footer-left-widget">
                    <h3 class="widget-title"><span>About Us</span></h3>
                    <div class="textwidget">
                        <p>Hidden Hills property with mountain and city view boast nine bedrooms including a master suite with private terrace and an entertainment. wing which includes a 20-seat theater.</p>
                    </div>
                </div>
                <div class="widget_text footer-left-widget">
                    <div class="textwidget custom-html-widget">
                        <ul class="footer-info">
                            <li> <i class="fa fa-home"></i> 15 Cliff St, New York NY 10038, USA</li>
                            <li> <i class="icon icon-phone2"></i> +1 212-602-9641</li>
                            <li><i class="fa fa-envelope"></i>info@example.com</li>
                        </ul>
                    </div>
                </div>
                <div class="footer-left-widget">
                    <div class="footer-social">
                        <ul class="xs-social-list xs-social-list-v6 digiqole-social-list">
                            <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-1 col-md-6">
                <div class="footer-widget footer-center-widget">
                    <h3 class="widget-title"><span>Popular Post</span></h3>
                    <div class="recent-posts-widget post-list-item">
                        <div class="post-tab-list">
                            <?php foreach ($dataHotsF as $data){?>
                            <div class="post-content media">
                                <div class="post-thumb">
                                    <a href="detail.php?id=<?php echo $data["id_new"]; ?>" rel="bookmark" title="Ratcliffe to be Director of nation talent Trump ignored">
                                        <img src="<?php echo $data['url_thumbnail']?>" alt="">
                                    </a>
                                </div>
                                <div class="post-info media-body">
                                    <span class="post-tag"> <a class="post-cat only-color" href="" style="color:#da1793"> <?php echo $data['tag']?></a> </span>
                                    <h4 class="post-title"><a href="detail.php?id=<?php echo $data["id_new"]; ?>"><?php echo $data['title']?></a></h4>
                                    <div class="post-meta"><span class="post-date"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $data['time']?></span></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="footer-widget footer-right-widget">
                    <div href="#">
                        <img width="285" height="375" src="img/fashion_1.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <p>© 2020, Digiqole - News Magazine WordPress Theme. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>