<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style type="text/css">
        button{
            margin-right: 20px;
            padding: 5px;
        }
        form{
            width: 600px;
            margin: auto;
            text-align: center;
        }
        div.form-group{
            width: 600px;
            height: 34px;
            margin: 5px;
            margin-bottom: 20px;
        }
        div.form-group input{
            height: 30px;
            width: 400px;
        }
        span{
            font: 18px bold;
            font-weight: bold;
            float: left;
            width: 150px;
            margin-right: 10px;
        }
        h1{
            text-align: center;
        }
        textarea{
            width: 400px;
            height: auto;


        }
        select{
            width: 408px;
            height: 35px;

        }
    </style>
</head>
<body>
<?php
//Kết nối databse
$con = mysqli_connect('localhost', 'root', '', 'tin_tuc_user');
//Viết câu SQL lấy tất cả dữ liệu trong bảng players
mysqli_set_charset($con,'utf8');
$sql="SELECT * FROM `menu` ORDER BY `id_menu`";
//Chạy câu SQL
$resultMenu=mysqli_query($con,$sql);
//Gắn dữ liệu lấy được vào mảng $data
while ($row=mysqli_fetch_assoc($resultMenu)) {
    $dataMenu[] = $row;
}
$sqlPhanloai="SELECT * FROM `phan_loai` ORDER BY `id_phanloai`";
$resultPhanloai= mysqli_query($con, $sqlPhanloai);
while ($row=mysqli_fetch_assoc($resultPhanloai)) {
    $dataPhanloai[] = $row;
}
?>
<form action="process.php" method="POST">
    <h1>Thêm bài viết</h1>
    <div class="form-group row">
        <input type="text" name="title"><span>Tiêu đề: </span>
    </div>
    <div class="form-group form-group row">
        <span style="float: left">Nội dung</span>
<!--        //<input type="text" name="content"><span> Nội dung: </span>-->
        <textarea type="text" name="content" style="width: 400px; height: auto;"></textarea>
    </div>
    <div class="form-group row">
        <input type="text" name="url_thumbnail"><span>Link ảnh: </span>
    </div>
    <div class="form-group row">
        <span>chuyên mục</span>
        <select name="tag" id="">
            <?php
                foreach ($dataMenu as $menu){?>
                      <option><?php  echo $menu['menu']; ?></option>
            <?php    }
            ?>
        </select>
<!--        <input type="text" name="tag"><span>thể loại: </span>-->
    </div>
    <div class="form-group">

        <span>Phân loại</span>
        <select name="categorie" id="">
            <?php
            foreach ($dataPhanloai as $phanloai){?>
                <option><?php  echo $phanloai['categorie']; ?></option>
            <?php    }
            ?>
        </select>
    </div>
    <div class="form-group">
        <button type="submit">Thêm</button>
        <button type="reset">Reset</button>
        <a href="index.php"><button type="button">Cancel</button></a>
    </div>
</form>
</body>
</html>