
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style type="text/css">
        table{
            width: 800px;
            margin: auto;
            text-align: center;
        }
        tr {
            border: 1px solid;
        }
        th {
            border: 1px solid;
        }
        td {
            border: 1px solid;
        }
        h1{
            text-align: center;
            color: red;
        }
        #button{
            margin: 2px;
            margin-right: 10px;
            float: right;
        }
    </style>
</head>
<body>
<?php
    //Kết nối databse
    $con = mysqli_connect('localhost', 'root', '', 'tin_tuc_user');
    //Viết câu SQL lấy tất cả dữ liệu trong bảng players
    mysqli_set_charset($con,'utf8');
    $sql="SELECT * FROM `post` ORDER BY `id_new`";
    //Chạy câu SQL
    $result=mysqli_query($con,$sql);
    //Gắn dữ liệu lấy được vào mảng $data
    while ($row=mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
?>
<table id="datatable" style="border: 1px solid">
    <h1>Danh sách tin tức</h1>
    <thead>
    <tr role="row">
        <th>số thứ tự</th>
        <th>ID</th>
        <th>Tiêu đề</th>
        <th>Nội dung</th>
        <th>link ảnh</th>
        <th>Thời gian</th>
        <th>Thể lọai</th>
        <th>phân loại</th>
        <th style="width: 7%;">Edit</th>
        <th style="width: 10%;">>Delete</th>
    </tr>
    </thead>
    <tbody>
    <?=
    $html = '';
    $stt=0;
    foreach ($data as $value) {
        $stt++;
        $html .= '
    <tr role="row">
        <td>'.$stt.'</td>
        <td>'.$value['id_new'].'</td>
        <td>'.$value['title'].'</td>
        <td>'.$value['content'].'</td>
        <td style="width: 70px; height: 50px;"><img style="width: 100%; height: 100%;" src=" ../'.$value['url_thumbnail'].'"></td>
        <td >'.$value['time'].'</td>
        <td>'.$value['tag'].'</td>
        <td>'.$value['categorie'].' </td>
        <td><a href="edit.php?id='.$value['id_new'].'">Edit</a></td>
        <td><a href="delete.php?id='.$value['id_new'].'"> Delete</a></td>
    </tr>';

    }
    echo $html;
    ?>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="8">
            <a href="add.php"><button id="button">Thêm tin tức</button></a>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>