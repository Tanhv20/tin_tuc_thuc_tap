
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style type="text/css">
        table{
            width: 800px;
            margin: auto;
            text-align: center;
        }
        tr {
            border: 1px solid;
        }
        th {
            border: 1px solid;
        }
        td {
            border: 1px solid;
        }
        h1{
            text-align: center;
            color: red;
        }
        #button{
            margin: 2px;
            margin-right: 10px;
            float: right;
        }
    </style>
</head>
<body>
<?php
//Kết nối databse
$con = mysqli_connect('localhost', 'root', '', 'tin_tuc_user');
//Viết câu SQL lấy tất cả dữ liệu trong bảng players
mysqli_set_charset($con,'utf8');
$sql="SELECT * FROM `user` ORDER BY `id`";
//Chạy câu SQL
$result_user=mysqli_query($con,$sql);
//Gắn dữ liệu lấy được vào mảng $data
while ($row=mysqli_fetch_assoc($result_user)) {
    $users[] = $row;
}
?>
<table id="datatable" style="border: 1px solid">
    <h1>Danh sách user</h1>
    <thead>
    <tr role="row">
        <th>số thứ tự</th>
        <th>ID</th>
        <th>USER NAME</th>
        <th>FULL NAME</th>
        <th style="width: 7%;">Edit</th>
        <th style="width: 10%;">>Delete</th>
    </tr>
    </thead>
    <tbody>
    <?=
    $html = '';
    $stt=0;
    foreach ($users as $value) {
        $stt++;
        $html .= '
    <tr role="row">
        <td>'.$stt.'</td>
        <td>'.$value['id'].'</td>
        <td>'.$value['user_name'].'</td>
        <td>'.$value['full_name'].'</td>
        <td><a href="edit.php?id='.$value['id'].'">Edit</a></td>
        <td><a href="delete_user.php?id='.$value['id'].'"> Delete</a></td>
    </tr>';

    }
    echo $html;
    ?>

    </tbody>

</table>
</body>
</html>
