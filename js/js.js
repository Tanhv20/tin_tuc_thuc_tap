$(document).ready(function () {
  
    $('#slider-sports').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      autoplaySpeed :30,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:3
          },
          1000:{
              items:4
          }
      }
  })
     $('#slider-sports-2').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          },
          1000:{
              items:4
          }
      }
  })

        // $( ".menu-item" ).on('click',function() {
        //     $(this).addClass("active");
        // });
        // $( ".menu-item" ).off('click',function() {
        //     $(this).remove("active");
        // });

    $(".menu .menu-item").on("click", function() {
        const self = $(this);
        self.parent().find(".active").removeClass("active");
        self.addClass("active")
    })

      $('#slider-page4').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:4
          }
      }
  })
  $('#news-1').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:1
          }
      }
  })


  $("input.form-control").css("display", "none");
  $(".nav-search-area").click(function(){
    $("input.form-control").toggle();
  });
  // var a=getFullYear();
  $('#date-time').append(moment().format("MM/DD/YYYY"))


    $("#responsive-menu").css("display", "none");
    $("#bars").click(function(){
        $("#responsive-menu").toggle();
    });
});




