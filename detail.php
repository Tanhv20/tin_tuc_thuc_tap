<?php require_once ('header.html.php');
require_once ('model.php');
$id = $_GET['id'];
$sql="SELECT * FROM `post` where `id_new`=$id";
//Chạy câu SQL
$resultnew=mysqli_query($con,$sql);

//Gắn dữ liệu lấy được vào mảng $data
while ($row=mysqli_fetch_assoc($resultnew)) {
    $detail[] = $row;
}
?>
<style>
    .new-page2 {
        padding-bottom: 40px;
        margin-top: 20px;
    }
    .title h2{
        text-align: justify;
    }
.img img{
    width: 100%;
    height: auto;
    object-fit: cover;
}
.content{
    margin-top: 20px;
    p{
        font-size: 16px;
        text-align: justify;
        line-height: 1.5;

    }
}

</style>
<body>
<contents>
    <div class="new-page2">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="detail">
                        <?php foreach ($detail as $get){?>
                        <div class="title"><h2 class=""><?php echo $get['title']?></h2> </div>
                        <div class="info">
                            <ul class="post-meta-info ">
                                <span class="post-author" style="list-style: none; text-decoration: none;"> <i class="fa fa-user"></i> <a href="" title="Posts by digiQoles" rel="author"><?php echo $get['author']?></a> </span>
                                <li style="list-style: none; text-decoration: none; padding-left: 20px;"> <i class="fa fa-clock-o"></i> <?php echo $get['time']?></h2></li>
                            </ul>
                        </div>
                        <div class="img"><img src="<?php echo $get['url_thumbnail']?>"></div>
                        <div class="content">
                            <p>
                                <?php echo $get['content']?></h2>
                            </p>
                        </div>
                        <?php }?>
                    </div>

                </div> <!-- col-md-8 -->
                <?php require_once ('sideBar.php');?>
            </div>
        </div>
    </div>
</contents>
<mail>
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-5 align-self-center">
                    <div class="footer-logo">
                        <a class="logo" href="" alt="DigiQole" data-ll-status="loaded">
                            <img class="img-fluid" src="img/logo-light.png">
                            <noscript><img class="img-fluid" src="img/logo-light.png"></noscript>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7">
                    <div class="ts-subscribe">

                        <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-464" method="post" data-id="464" data-name="footer newsletter">
                            <div class="mc4wp-form-fields">
                                <div class="footer-newsletter">
                                    <p> <i class="fa fa-paper-plane" aria-hidden="true"></i> <input type="email" name="EMAIL" placeholder="Your email" required=""></p>
                                    <input type="submit" value="Subscribe">
                                </div>
                            </div>
                            <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1597701941"><input type="hidden" name="_mc4wp_form_id" value="464"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                            <div class="mc4wp-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</mail>
<?php require_once ('footer.html.php')?>
</body>
</html>